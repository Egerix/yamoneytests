import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import static com.codeborne.selenide.Condition.matchText;
import static com.codeborne.selenide.Condition.visible;
import static com.codeborne.selenide.Selenide.*;

public class AuthorizationTest {

    private String password = "";

    @BeforeClass
    public void beforeClass() {
        password = System.getProperty("pass");
    }

    @AfterMethod
    public void after() {
        close();
    }

    @Test
    public void loginByUserWithoutWallet() {
        open("http://money.yandex.ru/", PromoPage.class).
                goToLogIn().
                loginViaLoginPassword("Egerix13WithoutWallet", password);
        $(".user__name").shouldHave(matchText("Egerix13Without"));
        $(".promo-screen").shouldBe(visible);
        assertNoJavascriptErrors();
    }

    @Test
    public void userCanLoginAndLogout() {
        ActionsPage actionsPage = open("http://money.yandex.ru/", PromoPage.class).
                goToLogIn().
                loginViaLoginPassword("Egerix13", password);
        actionsPage.getHeaderRight().shouldHave(matchText("Пополнить.*Снять.*Egerix13"));
        actionsPage.logOut();
        assertNoJavascriptErrors();
    }

    @Test
    public void userCanLoginByFb() {
        open("http://money.yandex.ru/", PromoPage.class).
                goToLogIn().
                loginViaFb("egerix13@yandex.ru", password).
                getHeaderRight().
                shouldHave(matchText("Пополнить.*Снять.*Egerix13"));
        assertNoJavascriptErrors();
    }

    @Test
    public void userCanLoginByGoogle() {
        open("http://money.yandex.ru/", PromoPage.class).
                goToLogIn().
                loginViaGoogle("egerix31@gmail.com", password).
                getHeaderRight().
                shouldHave(matchText("Пополнить.*Снять.*Egerix13"));
        assertNoJavascriptErrors();
    }

    @Test
    public void userCanLoginByMailRu() {
        open("http://money.yandex.ru/", PromoPage.class).
                goToLogIn().
                loginViaMailRu("egerix13@bk.ru", password).
                getHeaderRight().
                shouldHave(matchText("Пополнить.*Снять.*Egerix13"));
        assertNoJavascriptErrors();
    }

    @Test
    public void userCanLoginByTwitter() {
        open("http://money.yandex.ru/", PromoPage.class).
                goToLogIn().
                loginViaTwitter("Egerix13", password).
                getHeaderRight().
                shouldHave(matchText("Пополнить.*Снять.*Egerix13"));
        assertNoJavascriptErrors();
    }

    @Test
    public void userCanLoginByOk() {
        open("http://money.yandex.ru/", PromoPage.class).
                goToLogIn().
                loginViaOk("79027922112", password).
                getHeaderRight().
                shouldHave(matchText("Пополнить.*Снять.*Egerix13"));
        assertNoJavascriptErrors();
    }

    @Test
    public void userCanLoginByVk() {
        open("http://money.yandex.ru/", PromoPage.class).
                goToLogIn().
                loginViaVk("79027922112", password).
                getHeaderRight().
                shouldHave(matchText("Пополнить.*Снять.*Egerix13"));
        assertNoJavascriptErrors();
    }
}


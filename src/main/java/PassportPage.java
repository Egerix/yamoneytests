import org.openqa.selenium.By;

import static com.codeborne.selenide.Selectors.byText;
import static com.codeborne.selenide.Selenide.$;
import static com.codeborne.selenide.Selenide.page;
import static com.codeborne.selenide.Selenide.switchTo;

public class PassportPage {
    public ActionsPage loginViaLoginPassword(String login, String password) {
        $(By.name("login")).val(login);
        $(By.name("passwd")).val(password);
        $(byText("Войти")).click();
        return page(ActionsPage.class);
    }

    public ActionsPage loginViaFb(String email, String password) {
        $(".passport-Domik-SocialNetworks-Item_fb").click();
        switchTo().window(1);
        $("#email").val(email);
        $("#pass").val(password);
        $(By.name("login")).click();
        switchTo().window(0);
        return page(ActionsPage.class);
    }

    public ActionsPage loginViaGoogle(String email, String password) {
        $(".passport-Domik-SocialNetworks-Item_gg").click();
        switchTo().window(1);
        $(By.name("identifier")).val(email);
        $(byText("Далее")).click();
        $(By.name("password")).val(password);
        $(byText("Далее")).click();
        switchTo().window(0);
        return page(ActionsPage.class);
    }

    public ActionsPage loginViaMailRu(String email, String password) {
        $(".passport-Domik-SocialNetworks-Item_mr").click();
        switchTo().window(1);
        $("#login").val(email);
        $("#password").val(password);
        $(byText("Войти и разрешить")).click();
        switchTo().window(0);
        return page(ActionsPage.class);
    }

    public ActionsPage loginViaTwitter(String userNameOrEmail, String password) {
        $(".passport-Domik-SocialNetworks-Item_tw").click();
        switchTo().window(1);
        $("#username_or_email").val(userNameOrEmail);
        $("#password").val(password);
        $("#allow").click();
        switchTo().window(0);
        return page(ActionsPage.class);
    }

    public ActionsPage loginViaOk(String phoneOrEmailOrLogin, String password) {
        $(".passport-Domik-SocialNetworks-Item_ok").click();
        switchTo().window(1);
        $("#field_email").val(phoneOrEmailOrLogin);
        $("#field_password").val(password);
        $("[value=\"Войти\"]").click();
        switchTo().window(0);
        return page(ActionsPage.class);
    }

    public ActionsPage loginViaVk(String phoneOrEmail, String password) {
        $(".passport-Domik-SocialNetworks-Item_vk").click();
        switchTo().window(1);
        $(By.name("email")).val(phoneOrEmail);
        $(By.name("pass")).val(password);
        $("#install_allow").click();
        switchTo().window(0);
        return page(ActionsPage.class);
    }
}

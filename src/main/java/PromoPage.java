import static com.codeborne.selenide.Selenide.$;
import static com.codeborne.selenide.Selenide.page;

public class PromoPage {
    public PassportPage goToLogIn() {
        $("[title='Войти']").click();
        return page(PassportPage.class);
    }
}
